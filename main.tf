terraform {
    required_providers {
        oci = {
            source = "hashicorp/oci"
        }
    }
}

provider oci {
    region = var.region
}